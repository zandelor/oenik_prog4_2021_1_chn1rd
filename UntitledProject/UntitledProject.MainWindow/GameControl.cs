﻿namespace UntitledProject.MainWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using UntitledProject.Logic;
    using UntitledProject.Model;
    using UntitledProject.Renderer;

    /// <summary>
    /// Class for game control implementation.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private IGameLogic logic;
        private IGameModel model;
        private IGameRenderer renderer;
        private DispatcherTimer mainTimer;

        private bool leftPressed;
        private bool rightPressed;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModel(this.ActualWidth, this.ActualHeight);
            this.logic = new GameLogic(this.model, "UntitledProject.Logic.Levels.L00.lvl");
            this.renderer = new GameRenderer(this.model);
            this.leftPressed = false;
            this.rightPressed = false;

            System.Windows.Window win = System.Windows.Window.GetWindow(this);
            if (win != null)
            {
                win.KeyUp += this.Win_KeyUp;
                win.KeyDown += this.Win_KeyDown;
                this.mainTimer = new DispatcherTimer();
                this.mainTimer.Interval = TimeSpan.FromMilliseconds(50);
                this.mainTimer.Tick += this.MainTimer_Tick;
                this.mainTimer.Start();
            }

            this.InvalidateVisual();
        }

        private void Win_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftShift: this.logic.StopJump(); break;
                case Key.Left: this.leftPressed = false; break;
                case Key.Right: this.rightPressed = false; break;
            }
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            bool finished = false;
            switch (e.Key)
            {
                case Key.LeftShift: this.logic.Jump(); break;
                case Key.Left: this.leftPressed = true; break;
                case Key.Right: this.rightPressed = true; break;
            }

            this.InvalidateVisual();
            if (finished)
            {
                MessageBox.Show("yey! ");
            }
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            if (this.rightPressed)
            {
                this.logic.Move(0.2, 0);
            }
            else if (this.leftPressed)
            {
                this.logic.Move(-0.2, 0);
            }

            this.logic.OneTick();
            if (!this.logic.Alive)
            {
                this.InvalidateVisual();
                MessageBoxResult result = MessageBox.Show("You died, want to retry?", "dead :(", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.Cancel)
                {
                    Application.Current.Shutdown();
                }

                this.logic.Restart();
                this.leftPressed = false;
                this.rightPressed = false;
            }
            else if (this.logic.Finished)
            {
                this.InvalidateVisual();
                MessageBoxResult result = MessageBox.Show("You've won the game! Want to replay?", "Yey!", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    this.logic.Restart();
                    this.leftPressed = false;
                    this.rightPressed = false;
                }
                else
                {
                    Application.Current.Shutdown();
                }
            }

            this.InvalidateVisual();
        }
    }
}
