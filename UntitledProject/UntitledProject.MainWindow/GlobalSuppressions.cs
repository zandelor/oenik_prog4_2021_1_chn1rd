﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header is to be used throughout solution.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header is to be used throughout solution.", Scope = "namespace", Target = "~N:UntitledProject.MainWindow")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Renderer is tested and is correct.", Scope = "member", Target = "~M:UntitledProject.MainWindow.GameControl.OnRender(System.Windows.Media.DrawingContext)")]
