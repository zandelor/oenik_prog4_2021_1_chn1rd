﻿using System;

[assembly: CLSCompliant(false)]

namespace UntitledProject.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using UntitledProject.Model;

    /// <summary>
    /// Class describing game logic.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private IGameModel model;
        private int jumpCount;
        private int maxJumpCount;
        private int releaseJumpCountSub;
        private double vSpeed;
        private double maxVSpeed;
        private bool airborne;
        private Point playerStartPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">Game model to be used.</param>
        /// <param name="fname">File name to read level file.</param>
        public GameLogic(IGameModel model, string fname)
        {
            this.model = model;
            this.InitModel(fname);
        }

        /// <inheritdoc/>
        public bool Alive { get; private set; }

        /// <inheritdoc/>
        public bool Finished { get; private set; }

        /// <inheritdoc/>
        public void Restart()
        {
            this.model.Player = new Point(this.playerStartPoint.X, this.playerStartPoint.Y);
            this.Alive = true;
            this.Finished = false;
        }

        /// <inheritdoc/>
        public void Move(double dx, double dy)
        {
            double newX = this.model.Player.X + dx;
            double newY = this.model.Player.Y + dy;
            double leftX = newX - 0.4999;
            double rightX = newX + 0.49999;
            double botY = newY + 0.49999;
            double topY = newY - 0.49999;

            LevelElement element1;
            LevelElement element2;

            if (dx > 0)
            {
                element1 = this.model.Level[Convert.ToInt32(rightX), Convert.ToInt32(topY)];
                element2 = this.model.Level[Convert.ToInt32(rightX), Convert.ToInt32(botY)];
                if (this.TryMove(element1, element2, newX, newY))
                {
                    this.airborne = this.CheckBottom(leftX, rightX, botY);
                }
            }
            else if (dx < 0)
            {
                element1 = this.model.Level[Convert.ToInt32(leftX), Convert.ToInt32(topY)];
                element2 = this.model.Level[Convert.ToInt32(leftX), Convert.ToInt32(botY)];
                if (this.TryMove(element1, element2, newX, newY))
                {
                    this.airborne = this.CheckBottom(leftX, rightX, botY);
                }
            }
            else if (dy > 0)
            {
                element1 = this.model.Level[Convert.ToInt32(leftX), Convert.ToInt32(botY)];
                element2 = this.model.Level[Convert.ToInt32(rightX), Convert.ToInt32(botY)];
                if (!this.TryMove(element1, element2, newX, newY))
                {
                    this.model.Player = new Point(this.model.Player.X, Convert.ToInt32(botY) - 1);
                    this.airborne = false;
                    this.vSpeed = 0;
                }
            }
            else
            {
                element1 = this.model.Level[Convert.ToInt32(leftX), Convert.ToInt32(topY)];
                element2 = this.model.Level[Convert.ToInt32(rightX), Convert.ToInt32(topY)];
                if (!this.TryMove(element1, element2, newX, newY))
                {
                    this.model.Player = new Point(this.model.Player.X, Convert.ToInt32(topY) + 1);
                    this.jumpCount = 0;
                }
            }

            this.Finished = (Convert.ToInt32(rightX) == this.model.Finish.X && Convert.ToInt32(botY) == this.model.Finish.Y) ||
                (Convert.ToInt32(rightX) == this.model.Finish.X && Convert.ToInt32(topY) == this.model.Finish.Y) ||
                (Convert.ToInt32(leftX) == this.model.Finish.X && Convert.ToInt32(botY) == this.model.Finish.Y) ||
                (Convert.ToInt32(leftX) == this.model.Finish.X && Convert.ToInt32(topY) == this.model.Finish.Y);
        }

        /// <inheritdoc/>
        public void Jump()
        {
            if (!this.airborne)
            {
                this.jumpCount = this.maxJumpCount;
                this.vSpeed = this.maxVSpeed;
                this.airborne = true;
            }
        }

        /// <inheritdoc/>
        public void StopJump()
        {
            if (this.jumpCount > 0)
            {
                this.jumpCount -= this.releaseJumpCountSub;
                if (this.jumpCount >= 0)
                {
                    this.vSpeed = this.jumpCount * this.maxVSpeed / 10;
                }
                else
                {
                    this.vSpeed = 0;
                }
            }
        }

        /// <inheritdoc/>
        public void OneTick()
        {
            if (this.airborne)
            {
                if (this.jumpCount > 0)
                {
                    this.Move(0, -this.vSpeed);
                    this.vSpeed -= this.maxVSpeed / 10;
                    this.jumpCount--;
                }
                else
                {
                    this.Move(0, this.vSpeed);
                    if (this.vSpeed < this.maxVSpeed)
                    {
                        this.vSpeed += this.maxVSpeed / 10;
                    }
                }
            }
        }

        private void InitModel(string fname)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            StreamReader sr = new StreamReader(stream);
            string[] lines = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');

            int width = int.Parse(lines[0]);
            int height = int.Parse(lines[1]);

            this.model.Level = new LevelElement[width, height];
            this.model.TileSize = this.model.GameHeight / 8;

            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    char current = lines[y + 2][x];

                    switch (current)
                    {
                        case 'b': this.model.Level[x, y] = LevelElement.Block; break;
                        case ' ': this.model.Level[x, y] = LevelElement.Empty; break;
                        case 's': this.model.Level[x, y] = LevelElement.Spike; break;
                        case 'P': this.model.Level[x, y] = LevelElement.Empty; this.model.Player = new Point(x, y); break;
                        case 'f': this.model.Level[x, y] = LevelElement.Empty; this.model.Finish = new Point(x, y); break;
                    }
                }
            }

            this.Alive = true;
            this.Finished = false;
            this.jumpCount = 0;
            this.maxJumpCount = 10;
            this.releaseJumpCountSub = 7;
            this.vSpeed = 0;
            this.maxVSpeed = 0.4;
            this.airborne = false;
            this.playerStartPoint = new Point(this.model.Player.X, this.model.Player.Y);
        }

        private bool TryMove(LevelElement element1, LevelElement element2, double newX, double newY)
        {
            if (element1 == LevelElement.Empty && element2 == LevelElement.Empty)
            {
                this.model.Player = new Point(newX, newY);
                return true;
            }
            else if (element1 == LevelElement.Spike || element2 == LevelElement.Spike)
            {
                this.model.Player = new Point(newX, newY);
                this.Alive = false;
            }

            return false;
        }

        private bool CheckBottom(double leftX, double rightX, double botY)
        {
            if (this.model.Level[Convert.ToInt32(leftX), Convert.ToInt32(botY)] == LevelElement.Empty
                && this.model.Level[Convert.ToInt32(rightX), Convert.ToInt32(botY)] == LevelElement.Empty)
            {
                return true;
            }

            return false;
        }
    }
}
