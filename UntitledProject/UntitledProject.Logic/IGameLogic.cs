﻿namespace UntitledProject.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for public GameLogic properties and methods.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Gets a value indicating whether player is alive.
        /// </summary>
        public bool Alive { get; }

        /// <summary>
        /// Gets a value indicating whether player has reached finish portal.
        /// </summary>
        public bool Finished { get; }

        /// <summary>
        /// Restarts level.
        /// </summary>
        public void Restart();

        /// <summary>
        /// Moves the player by described amount.
        /// </summary>
        /// <param name="dx">Move player by dx amount horizontally.</param>
        /// <param name="dy">Move player by dy amount vertically.</param>
        public void Move(double dx, double dy);

        /// <summary>
        /// Makes the player jump.
        /// </summary>
        public void Jump();

        /// <summary>
        /// Makes the player stop jumping.
        /// </summary>
        public void StopJump();

        /// <summary>
        /// Moves the game 1 tick forward in time.
        /// </summary>
        public void OneTick();
    }
}
