﻿using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header is to be used throughout solution.", Scope = "namespace", Target = "~N:UntitledProject.Logic")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header is to be used throughout solution.")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposal is left to gc.", Scope = "member", Target = "~M:UntitledProject.Logic.GameLogic.InitModel(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Conversion is intended behaviour.", Scope = "member", Target = "~M:UntitledProject.Logic.GameLogic.InitModel(System.String)")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "2d array is correct.", Scope = "member", Target = "~M:UntitledProject.Logic.GameLogic.InitModel(System.String)")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "Intent is clean.", Scope = "member", Target = "~M:UntitledProject.Logic.GameLogic.InitModel(System.String)")]
