﻿namespace UntitledProject.Model
{
    /// <summary>
    /// Enum for possible level elements.
    /// </summary>
    public enum LevelElement
    {
        /// <summary>
        /// Solic block level element.
        /// </summary>
        Block,

        /// <summary>
        /// Killing block level element>
        /// </summary>
        Spike,

        /// <summary>
        /// Empty level element.
        /// </summary>
        Empty,
    }
}
