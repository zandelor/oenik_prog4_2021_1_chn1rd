﻿namespace UntitledProject.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for public GameModel properties and methods.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets 2d array representation of the game level.
        /// </summary>
        public LevelElement[,] Level { get; set; }

        /// <summary>
        /// Gets or sets the player point representation.
        /// </summary>
        public Point Player { get; set; }

        /// <summary>
        /// Gets or sets the finish portal point representation.
        /// </summary>
        public Point Finish { get; set; }

        /// <summary>
        /// Gets the width of the game window.
        /// </summary>
        public double GameWidth { get; }

        /// <summary>
        /// Gets the height of the game window.
        /// </summary>
        public double GameHeight { get; }

        /// <summary>
        /// Gets or sets the tile size.
        /// </summary>
        public double TileSize { get; set; }
    }
}
