﻿using System;

[assembly: CLSCompliant(false)]

namespace UntitledProject.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Game model class representation.
    /// </summary>
    public class GameModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="width">Width of the game window.</param>
        /// <param name="height">Height of the game window.</param>
        public GameModel(double width, double height)
        {
            this.GameWidth = width;
            this.GameHeight = height;
        }

        /// <inheritdoc/>
        public LevelElement[,] Level { get; set; }

        /// <inheritdoc/>
        public Point Player { get; set; }

        /// <inheritdoc/>
        public Point Finish { get; set; }

        /// <inheritdoc/>
        public double GameWidth { get; private set; }

        /// <inheritdoc/>
        public double GameHeight { get; private set; }

        /// <inheritdoc/>
        public double TileSize { get; set; }
    }
}
