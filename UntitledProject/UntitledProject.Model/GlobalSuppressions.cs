﻿using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header is to be used throughout solution.", Scope = "namespace", Target = "~N:UntitledProject.Model")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header is to be used throughout solution.")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Multidimensional array is correct for this solution.", Scope = "member", Target = "~P:UntitledProject.Model.IGameModel.Level")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Array return is tested and is correct.", Scope = "member", Target = "~P:UntitledProject.Model.GameModel.Level")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Array return is tested and is correct.", Scope = "member", Target = "~P:UntitledProject.Model.IGameModel.Level")]
