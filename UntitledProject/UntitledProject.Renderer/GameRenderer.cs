﻿using System;

[assembly: CLSCompliant(false)]

namespace UntitledProject.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using UntitledProject.Model;

    /// <summary>
    /// Class for game renderer.
    /// </summary>
    public class GameRenderer : IGameRenderer
    {
        private IGameModel model;
        private Drawing oldBackground;
        private DrawingGroup oldLevel;
        private Drawing oldPlayer;
        private Drawing emptyFinsish;
        private Drawing finish;
        private Point oldPLayerPosition;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();
        private int levelHeight;
        private int currentScreen;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">Model to render.</param>
        public GameRenderer(IGameModel model)
        {
            this.model = model;
            this.currentScreen = 0;
            this.levelHeight = this.model.Level.GetLength(1);
        }

        private Brush PlayerBrush
        {
            get { return this.GetBrush("UntitledProject.Renderer.Images.player.bmp", false); }
        }

        private Brush ExitBrush
        {
            get { return this.GetBrush("UntitledProject.Renderer.Images.exit.bmp", false); }
        }

        private Brush WallBrush
        {
            get { return this.GetBrush("UntitledProject.Renderer.Images.wall.bmp", true); }
        }

        private Brush SpikeBrush
        {
            get { return this.GetBrush("UntitledProject.Renderer.Images.spike.bmp", true); }
        }

        /// <inheritdoc/>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetLevel());
            dg.Children.Add(this.GetFinish());
            dg.Children.Add(this.GetPlayer());

            return dg;
        }

        private int GetCurrentScreen()
        {
            return (int)((this.model.Player.X + 0.0001) / 8);
        }

        private Brush GetBrush(string fname, bool isTiled)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                if (isTiled)
                {
                    ib.TileMode = TileMode.Tile;
                    ib.Viewport = new Rect(0, 0, this.model.TileSize, this.model.TileSize);
                    ib.ViewportUnits = BrushMappingMode.Absolute;
                }

                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        private Drawing GetBackground()
        {
            if (this.oldBackground == null)
            {
                Geometry g = new RectangleGeometry(new Rect(0, 0, this.model.GameHeight, this.model.GameHeight));
                this.oldBackground = new GeometryDrawing(Brushes.Cyan, null, g);
            }

            return this.oldBackground;
        }

        private Drawing GetLevel()
        {
            if (this.oldLevel == null || this.GetCurrentScreen() != this.currentScreen)
            {
                this.currentScreen = this.GetCurrentScreen();
                GeometryGroup wallGroup = new GeometryGroup();
                GeometryGroup spikeGroup = new GeometryGroup();
                int t = this.currentScreen * this.levelHeight;
                for (int x = t; x < t + this.levelHeight; ++x)
                {
                    for (int y = 0; y < this.levelHeight; ++y)
                    {
                        if (this.model.Level[x, y] == LevelElement.Block)
                        {
                            Geometry box = new RectangleGeometry(new Rect((x % this.levelHeight) * this.model.TileSize, (y % this.levelHeight) * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            wallGroup.Children.Add(box);
                        }
                        else if (this.model.Level[x, y] == LevelElement.Spike)
                        {
                            Geometry box = new RectangleGeometry(new Rect((x % this.levelHeight) * this.model.TileSize, (y % this.levelHeight) * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            spikeGroup.Children.Add(box);
                        }
                    }
                }

                this.oldLevel = new DrawingGroup();
                this.oldLevel.Children.Add(new GeometryDrawing(this.WallBrush, null, wallGroup));
                this.oldLevel.Children.Add(new GeometryDrawing(this.SpikeBrush, null, spikeGroup));
            }

            return this.oldLevel;
        }

        private Drawing GetFinish()
        {
            if (this.emptyFinsish == null || this.finish == null)
            {
                Geometry g = Geometry.Empty;
                this.emptyFinsish = new GeometryDrawing(this.ExitBrush, null, g);
                g = new RectangleGeometry(new Rect((this.model.Finish.X % this.levelHeight) * this.model.TileSize, (this.model.Finish.Y % this.levelHeight) * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                this.finish = new GeometryDrawing(this.ExitBrush, null, g);
            }

            if ((int)(this.model.Finish.X / this.levelHeight) != this.currentScreen)
            {
                return this.emptyFinsish;
            }
            else
            {
                return this.finish;
            }
        }

        private Drawing GetPlayer()
        {
            if (this.oldPlayer == null || this.oldPLayerPosition != this.model.Player)
            {
                Geometry g = new RectangleGeometry(new Rect((this.model.Player.X - (this.currentScreen * this.levelHeight)) * this.model.TileSize, (this.model.Player.Y % this.levelHeight) * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                this.oldPlayer = new GeometryDrawing(this.PlayerBrush, null, g);
                this.oldPLayerPosition = this.model.Player;
            }

            return this.oldPlayer;
        }
    }
}
