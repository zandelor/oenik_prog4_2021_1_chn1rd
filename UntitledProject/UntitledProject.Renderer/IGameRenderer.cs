﻿namespace UntitledProject.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Interface for public GameRenderer methods.
    /// </summary>
    public interface IGameRenderer
    {
        /// <summary>
        /// Builds the drawing to be drawn on the screen.
        /// </summary>
        /// <returns>Drawing containing all content to be displayed.</returns>
        public Drawing BuildDrawing();
    }
}
