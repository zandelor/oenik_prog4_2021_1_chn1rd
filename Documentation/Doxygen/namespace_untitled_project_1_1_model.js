var namespace_untitled_project_1_1_model =
[
    [ "GameModel", "class_untitled_project_1_1_model_1_1_game_model.html", "class_untitled_project_1_1_model_1_1_game_model" ],
    [ "IGameModel", "interface_untitled_project_1_1_model_1_1_i_game_model.html", "interface_untitled_project_1_1_model_1_1_i_game_model" ],
    [ "LevelElement", "namespace_untitled_project_1_1_model.html#ae34db7f33068c80267f4cf4b1bd1b5a9", [
      [ "Block", "namespace_untitled_project_1_1_model.html#ae34db7f33068c80267f4cf4b1bd1b5a9ae1e4c8c9ccd9fc39c391da4bcd093fb2", null ],
      [ "Spike", "namespace_untitled_project_1_1_model.html#ae34db7f33068c80267f4cf4b1bd1b5a9a01d02e67374273af80ed14d9a96b28cf", null ],
      [ "Empty", "namespace_untitled_project_1_1_model.html#ae34db7f33068c80267f4cf4b1bd1b5a9ace2c8aed9c2fa0cfbed56cbda4d8bf07", null ]
    ] ]
];