var annotated_dup =
[
    [ "UntitledProject", "namespace_untitled_project.html", [
      [ "Logic", "namespace_untitled_project_1_1_logic.html", [
        [ "GameLogic", "class_untitled_project_1_1_logic_1_1_game_logic.html", "class_untitled_project_1_1_logic_1_1_game_logic" ],
        [ "IGameLogic", "interface_untitled_project_1_1_logic_1_1_i_game_logic.html", "interface_untitled_project_1_1_logic_1_1_i_game_logic" ]
      ] ],
      [ "MainWindow", "namespace_untitled_project_1_1_main_window.html", [
        [ "App", "class_untitled_project_1_1_main_window_1_1_app.html", "class_untitled_project_1_1_main_window_1_1_app" ],
        [ "GameControl", "class_untitled_project_1_1_main_window_1_1_game_control.html", "class_untitled_project_1_1_main_window_1_1_game_control" ],
        [ "Window", "class_untitled_project_1_1_main_window_1_1_window.html", "class_untitled_project_1_1_main_window_1_1_window" ]
      ] ],
      [ "Model", "namespace_untitled_project_1_1_model.html", [
        [ "GameModel", "class_untitled_project_1_1_model_1_1_game_model.html", "class_untitled_project_1_1_model_1_1_game_model" ],
        [ "IGameModel", "interface_untitled_project_1_1_model_1_1_i_game_model.html", "interface_untitled_project_1_1_model_1_1_i_game_model" ]
      ] ],
      [ "Renderer", "namespace_untitled_project_1_1_renderer.html", [
        [ "GameRenderer", "class_untitled_project_1_1_renderer_1_1_game_renderer.html", "class_untitled_project_1_1_renderer_1_1_game_renderer" ],
        [ "IGameRenderer", "interface_untitled_project_1_1_renderer_1_1_i_game_renderer.html", "interface_untitled_project_1_1_renderer_1_1_i_game_renderer" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];