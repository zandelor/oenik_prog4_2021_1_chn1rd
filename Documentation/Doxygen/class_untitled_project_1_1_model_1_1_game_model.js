var class_untitled_project_1_1_model_1_1_game_model =
[
    [ "GameModel", "class_untitled_project_1_1_model_1_1_game_model.html#adbff297e688b1eb30bc337dd9a979914", null ],
    [ "Finish", "class_untitled_project_1_1_model_1_1_game_model.html#a478fc963417692d222d7b13eb720f279", null ],
    [ "GameHeight", "class_untitled_project_1_1_model_1_1_game_model.html#a02c9fb7d540874d553ef0ad66764fd55", null ],
    [ "GameWidth", "class_untitled_project_1_1_model_1_1_game_model.html#a52f8d41e73689068165e243443319040", null ],
    [ "Level", "class_untitled_project_1_1_model_1_1_game_model.html#a88cc70f98f6cf2ce2c94b0e45611e37f", null ],
    [ "Player", "class_untitled_project_1_1_model_1_1_game_model.html#a9c531ed2c47c850c5ba6cc2e9848b979", null ],
    [ "TileSize", "class_untitled_project_1_1_model_1_1_game_model.html#ab9431e4481c62591f71fd0f300167501", null ]
];