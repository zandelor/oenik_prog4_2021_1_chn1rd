var hierarchy =
[
    [ "Application", null, [
      [ "UntitledProject.MainWindow.App", "class_untitled_project_1_1_main_window_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "UntitledProject.MainWindow.App", "class_untitled_project_1_1_main_window_1_1_app.html", null ],
      [ "UntitledProject.MainWindow.App", "class_untitled_project_1_1_main_window_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "UntitledProject.MainWindow.GameControl", "class_untitled_project_1_1_main_window_1_1_game_control.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "UntitledProject.MainWindow.Window", "class_untitled_project_1_1_main_window_1_1_window.html", null ],
      [ "UntitledProject.MainWindow.Window", "class_untitled_project_1_1_main_window_1_1_window.html", null ]
    ] ],
    [ "UntitledProject.Logic.IGameLogic", "interface_untitled_project_1_1_logic_1_1_i_game_logic.html", [
      [ "UntitledProject.Logic.GameLogic", "class_untitled_project_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "UntitledProject.Model.IGameModel", "interface_untitled_project_1_1_model_1_1_i_game_model.html", [
      [ "UntitledProject.Model.GameModel", "class_untitled_project_1_1_model_1_1_game_model.html", null ]
    ] ],
    [ "UntitledProject.Renderer.IGameRenderer", "interface_untitled_project_1_1_renderer_1_1_i_game_renderer.html", [
      [ "UntitledProject.Renderer.GameRenderer", "class_untitled_project_1_1_renderer_1_1_game_renderer.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "UntitledProject.MainWindow.Window", "class_untitled_project_1_1_main_window_1_1_window.html", null ],
      [ "UntitledProject.MainWindow.Window", "class_untitled_project_1_1_main_window_1_1_window.html", null ],
      [ "UntitledProject.MainWindow.Window", "class_untitled_project_1_1_main_window_1_1_window.html", null ]
    ] ]
];